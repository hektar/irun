import React, { Component } from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import TrackingScreen from './screens/tracks/TrackingScreen';
import FriendListScreen from './screens/friends/FriendListScreen';
import FriendStatisticScreen from './screens/friends/FriendStatisticScreen';
import FriendScreen from './screens/friends/FriendScreen';
import FriendRequestsScreen from './screens/friends/FriendRequestsScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import LoginScreen from './screens/login/LoginScreen';
import RegisterScreen from './screens/login/RegisterScreen';
import ProfileScreen from './screens/profile/ProfileScreen';
import ActivityScreen from './screens/activity/ActivityScreen';
import StatisticScreen from './screens/activity/StatisticScreen';
import MyTracksScreen from './screens/tracks/MyTracksScreen';
import TrackCompareScreen from './screens/tracks/TrackCompareScreen';
import TracksOfFriendsScreen from './screens/tracks/TracksOfFriendsScreen';
import MyTrackMapScreen from './screens/tracks/MyTrackMapScreen';
import HomeScreen from './screens/HomeScreen';
import RankingScreen from './screens/ranking/RankingScreen';
import RankingTracksScreen from './screens/ranking/RankingTracksScreen';
import Drawer from './Drawer';
import Colors from './config/Colors';

//erstelle den Stack mit Screens, die dem authentifizierten User dargestellt werden können
const IRunStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Tracking: {
      screen: TrackingScreen,
    },
    MyTracks: {
      screen: MyTracksScreen,
    },
    TrackCompare: {
      screen: TrackCompareScreen,
    },
    TracksOfFriends: {
      screen: TracksOfFriendsScreen,
    },
    MyTrackMap: {
      screen: MyTrackMapScreen,
    },
    Ranking: {
      screen: RankingScreen,
    },
    RankingTracks: {
      screen: RankingTracksScreen,
    },
    Friendlist: {
      screen: FriendListScreen,
    },
    FriendStatistic: {
      screen: FriendStatisticScreen,
    },
    Friend: {
      screen: FriendScreen,
    },
    FriendRequests: {
      screen: FriendRequestsScreen,
    },
    Profile: {
      screen: ProfileScreen,
    },
    Activity: {
      screen: ActivityScreen,
    },
    Statistic: {
      screen: StatisticScreen,
    },
  },
  //optionen für den Stack
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: Colors.primary,
      },
    },
  },
);

//Drawer für alle Screens außer Home ausschalten
IRunStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }

  return {
    drawerLockMode,
  };
};

const IRunDrawerNavigator = createDrawerNavigator(
  {
    ApplicationStack: IRunStack,
  },
  {
    contentComponent: Drawer,
    drawerBackgroundColor: Colors.primaryDark,
    drawerWidth: 128,
    overlayColor: 'rgba(0, 0, 0, 0.7)',
  },
);

// erstelle den Stack mit Screens, die dem unauthentifizierten User dargestellt werden können
const AuthStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
  },
  Register: {
    screen: RegisterScreen,
  },
});

// erstelle den Appcontainer
const IRunApp = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: IRunDrawerNavigator, //IRunStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);

export default class App extends Component {
  componentDidMount() {
    console.log('iRun App componentDidMount');
  }

  componentWillUnmount() {
    console.log('iRun App componentWillUnmount');
  }

  render() {
    return <IRunApp />;
  }
}
