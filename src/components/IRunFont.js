import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontConfig from '../config/selection.json';
export default createIconSetFromIcoMoon(
    fontConfig,
    'irun',
    'irun.ttf'
);