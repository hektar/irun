import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class SignalMeter extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={this.barStyle(1)} />
                <View style={this.barStyle(2)} />
                <View style={this.barStyle(3)} />
            </View>
        );
    }

    barStyle = (bar) => {
        return {
            borderRadius: 2,
            width: '20%',
            height: `${25 * bar}%`,
            backgroundColor: this.props.strength >= 25 * (bar - 1) + 10 ? 'green' : 'lightgrey',
        }
    }

}

const styles = StyleSheet.create({
    container: {
        width: 24,
        height: 24,
        padding: 5,
        flexDirection: "row",
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        borderRadius: 2
    }
});