import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';

const levels = {
    Neuling: 0,
    Anfaenger: 1,
    Lehrling: 2,
    read: 4
};

export default class Level extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text >
                    {this._getLevel(this.props.exp)}
                </Text>
            </View>
        );
    }

    _getLevel = (exp) => {
        // Entnommen aus der Tabelle 4 in der BA
        if (this._between(exp, 0, 2500)) return "Neuling"
        if (this._between(exp, 2501, 5630)) return "Anfänger"
        if (this._between(exp, 5631, 9150)) return "Lehrling"
        if (this._between(exp, 9151, 13110)) return "Kompetenter"
        if (this._between(exp, 13111, 17560)) return "Gewandter"
        if (this._between(exp, 17561, 22570)) return "Super Athlet"
        if (this._between(exp, 22571, 28200)) return "Semiprofi"
        if (exp >= 28201) return "Profi"
    }

    _between = (x, min, max) => {
        return x >= min && x <= max;
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});