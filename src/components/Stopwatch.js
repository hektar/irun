import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');

class Stopwatch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: 0,
      now: 0,
    };
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  start = () => {
    const now = new Date().getTime();
    this.setState({
      start: now,
      now: now,
    });
    this.timer = setInterval(() => {
      this.setState({ now: new Date().getTime() });
    }, 100);
  };

  stop = () => {
    clearInterval(this.timer);
  };

  reset = () => {
    this.setState({
      start: 0,
      now: 0,
    });
  };

  getTimeInMilliseconds = () => {
    const { now, start } = this.state;
    return now - start;
  }

  getTime = () => {
    const { now, start } = this.state;
    const pad = n => (n < 10 ? '0' + n : n);
    const duration = moment.duration(now - start);
    const centiseconds = Math.floor(duration.milliseconds() / 10);
    return `${pad(duration.minutes())}:${pad(duration.seconds())},${pad(
      centiseconds,
    )}`;
  };

  render() {
    const { now, start, laps } = this.state;
    const timer = now - start;

    const pad = n => (n < 10 ? '0' + n : n);
    const duration = moment.duration(timer);
    const centiseconds = Math.floor(duration.milliseconds() / 10);

    return (
      <View style={styles.timerContainer}>
        <Text style={styles.timer}>{pad(duration.minutes())}:</Text>
        <Text style={styles.timer}>{pad(duration.seconds())},</Text>
        <Text style={styles.timer}>{pad(centiseconds)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  timerContainer: {
    flexDirection: 'row',
  },
  timer: {
    color: '#aaaaaa',
    fontSize: 32,
    fontWeight: '200',
    alignSelf: 'center',
    width: 50,
  },
});

export default Stopwatch;
