import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Alert
} from "react-native";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";
import moment from "moment";
import "moment/locale/de";
moment.locale("de");

export default class ProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Profil",
      headerBackTitle: null
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      user: null
    };
  }

  componentDidMount() {
    // console.log(this.props.user);
    this.fetchProfileData(this.props.user);
  }

  fetchProfileData = user => {
    // fetch(
    //   `${Constants.WEB_URL}${Constants.PROFILE_URL}?${user.SESSION_NAME}=${
    //     user.SESSION_ID
    //   }&APPID=App`
    // )
    //   .then(res => {
    //     console.log(res);
    //     return res.json();
    //   })
    //   .then(profile => {
    //     console.log("PROFILE", profile);
    //     this.setState({ user: profile });
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     Alert.alert(
    //       "Fehler",
    //       "Bitte melden Sie sich erneut an",
    //       [
    //         {
    //           text: "OK",
    //           onPress: () => {
    //             logout();
    //           }
    //         }
    //       ],
    //       { cancelable: false }
    //     );
    //   });
  };

  render() {
    let altImg = {
      uri: `https://ui-avatars.com/api/?name=Tolga&length=1`
    };
    return (
      <ParallaxScrollView
        parallaxHeaderHeight={250}
        contentBackgroundColor="#ccc"
        bounces={false}
        renderBackground={() => (
          <Image
            style={styles.imgHeader}
            source={require("../../assets/img/login_bg.png")}
          />
        )}
        renderForeground={() => (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "rgba(0,0,0,0.75)"
            }}
          >
            <Image style={styles.imgProfile} source={altImg} />
            <Text style={{ color: "#FFF", marginTop: 16, fontSize: 20 }}>
              TOLGA KOCER
            </Text>
          </View>
        )}
      >
        <View
          style={{
            padding: 12,
            backgroundColor: "#ccc"
          }}
        >
          <View
            style={{
              height: 265,
              backgroundColor: "#fff",
              shadowColor: "#000",
              shadowOffset: {
                width: 4,
                height: 4
              },
              shadowOpacity: 0.32,
              shadowRadius: 5.46,
              borderRadius: 8,
              elevation: 9
            }}
          >
            <Text style={{ margin: 16 }}>INFO</Text>
            <View
              style={{
                height: StyleSheet.hairlineWidth,
                width: "100%",
                backgroundColor: "#ccc"
              }}
            />
          </View>
          <View
            style={{
              height: 265,
              marginTop: 16,
              backgroundColor: "#fff",
              shadowColor: "#000",
              shadowOffset: {
                width: 4,
                height: 4
              },
              shadowOpacity: 0.32,
              shadowRadius: 5.46,
              borderRadius: 8,
              elevation: 9
            }}
          >
            <Text style={{ margin: 16 }}>Letzte Aktivitäten</Text>
            <View
              style={{
                height: StyleSheet.hairlineWidth,
                width: "100%",
                backgroundColor: "#ccc"
              }}
            />
          </View>
          {/* <View style={styles.header}>
            <Icons name="information" size={30} color="#3498db" />
            <Text style={{ fontSize: 20, color: "#3498db", marginLeft: 16 }}>
              INFO
            </Text>
          </View>
          <View
            style={{
              padding: 16,
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontSize: 16, color: "#bdc3c7" }}>
              Vor- und Nachname:
            </Text>
            <Text style={{ fontSize: 16, color: "#7f8c8d" }}>Huhu</Text>
          </View>
          <View
            style={{
              padding: 16,
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontSize: 16, color: "#bdc3c7" }}>E-Mail:</Text>
            <Text style={{ fontSize: 16, color: "#7f8c8d" }}>Janms</Text>
          </View>
          <View
            style={{
              padding: 16,
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontSize: 16, color: "#bdc3c7" }}>
              Ausbildungsbeginn:
            </Text>
            <Text style={{ fontSize: 16, color: "#7f8c8d" }}>
              {moment().format("DD.MM.YYYY")}
            </Text>
          </View>
          <View
            style={{
              padding: 16,
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontSize: 16, color: "#bdc3c7" }}>
              Ausbildungsende:
            </Text>
            <Text style={{ fontSize: 16, color: "#7f8c8d" }}>
              {moment().format("DD.MM.YYYY")}
            </Text>
          </View>
          <View style={styles.header}>
            <Icons name="file-document" size={30} color="#3498db" />
            <Text style={{ fontSize: 20, color: "#3498db", marginLeft: 16 }}>
              DOKUMENTE
            </Text>
          </View> */}
        </View>
      </ParallaxScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 72,
    flexDirection: "row",
    backgroundColor: "#ecf0f1",
    // justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 16
  },
  imgHeader: {
    height: "100%",
    width: "100%",
    resizeMode: "cover",
    backgroundColor: "#7f8c8d"
  },
  imgProfile: {
    height: 128,
    width: 128,
    borderRadius: 64,
    borderWidth: 4,
    borderColor: "#7f8c8d",
    resizeMode: "cover"
  }
});
