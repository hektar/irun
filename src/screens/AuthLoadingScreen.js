import React, {Component} from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  View,
  StatusBar,
  StyleSheet,
  Image,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Keys from '../config/StorageKeys';

export default class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this._checkUserAuth();
  }

  // TODO: Prüfe, ob der User sich bereits angemeldet hat
  _checkUserAuth = () => {
    setTimeout(async () => {
      const user = await AsyncStorage.getItem(Keys.USER_KEY);

      // this.props.navigation.navigate("Track");

      // Starte Home, wenn der User bereits existiert und schicke die Daten als Parameter mit
      this.props.navigation.navigate(user ? 'Home' : 'Auth', {
        user: JSON.parse(user),
      });
    }, 2000);
  };

  render() {
    return (
      <ImageBackground
        source={require('../assets/img/login_bg.png')}
        style={{width: '100%', height: '100%'}}>
        <StatusBar hidden={true} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={[styles.image]}
            source={require('../assets/img/logo.png')}
          />
          <ActivityIndicator size="large" color="#f5f6fa" />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#273c75',
  },
});
