import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  RefreshControl,
  Alert,
  Dimensions
} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import Colors from "../../config/Colors";
import API from "../../config/Api";
import NoFriendRequests from "../../404Error/NoFriendRequests";
import * as Animatable from "react-native-animatable";

export default class FriendRequestsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Freundschaftsanfragen",
      headerBackTitle: null
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam("user");
    this.refreshFriendList = props.navigation.getParam("refreshFriendList");
    this.itemRefs = {};
    this.state = {
      refreshing: false,
      friendRequests: props.navigation.getParam("friendRequests", [])
    };

    console.log(this.state.friendRequests);
  }

  render() {
    if (this.state.friendRequests.length == 0) {
      return <NoFriendRequests />;
    } else {
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this._fetchFriendRequests();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          data={this.state.friendRequests}
          style={styles.container}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          ListHeaderComponent={this._renderHeader}
          keyExtractor={this._keyExtractor}
        />
      );
    }
  }

  _fetchFriendRequests = () => {
    fetch(`${API.BASE_URL}${API.USER_FRIENDS}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${this.user.token}`
      }
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) return resp.json();
      })
      .then(json => {
        console.log(json);
        if (json) {
          this.setState({
            friendRequests: json.friendRequests,
            refreshing: false
          });
        }
      })
      .catch(err => console.log(err));
  };

  _renderItem = ({ item }) => {
    let altImg = {
      uri: `https://ui-avatars.com/api/?name=${item.username}&length=1`
    };
    let imgContainer = <Image style={styles.avatar} source={altImg} />;
    return (
      <Animatable.View
        style={styles.itemContainer}
        ref={ref => {
          this.itemRefs[item._id] = ref;
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {imgContainer}
          <Text style={styles.username}>{item.username}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => {
              this._acceptRequest(item);
            }}
          >
            <AntDesign name="checkcircleo" size={28} color={Colors.accent} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ marginLeft: 24 }}
            onPress={() => {
              this._declineRequest(item);
            }}
          >
            <AntDesign name="closecircleo" size={28} color={Colors.lightRed} />
          </TouchableOpacity>
        </View>
      </Animatable.View>
    );
  };

  _acceptRequest = friend => {
    let body = {
      id: friend._id,
      username: friend.username
    };

    fetch(`${API.BASE_URL}${API.USER_ACCEPT_FRIENDREQUEST}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${this.user.token}`
      },
      body: JSON.stringify(body)
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          this.itemRefs[friend._id].bounceOut(800).then(endState => {
            if (endState.finished) {
              this._fetchFriendRequests();
              this.refreshFriendList();
            }
          });
        }
      })
      .catch(err => console.log(err));
  };

  _declineRequest = friend => {
    Alert.alert(
      "Bitte Bestätigen",
      "Willst du diese Freundschaftsanfrage wirklich ablehnen?",
      [
        {
          text: "Ablehnen",
          onPress: () => {
            let body = {
              id: friend._id,
              username: friend.username
            };
            fetch(`${API.BASE_URL}${API.USER_DECLINE_FRIENDREQUEST}`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Token ${this.user.token}`
              },
              body: JSON.stringify(body)
            })
              .then(resp => {
                console.log(resp);
                if (resp.status == 200) {
                  this.itemRefs[friend._id].bounceOut(800).then(endState => {
                    if (endState.finished) {
                      this._fetchFriendRequests();
                      this.refreshFriendList();
                    }
                  });
                }
              })
              .catch(err => console.log(err));

          }
        },
        {
          text: "Abbrechen",
          onPress: () => { },
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  avatar: {
    height: 48,
    width: 48,
    borderRadius: 24
  },
  itemContainer: {
    flexDirection: "row",
    padding: 16,
    alignItems: "center",
    justifyContent: "space-between"
  },
  buttonContainer: {
    flexDirection: "row"
  },
  username: {
    marginLeft: 16
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: "#ccc"
  }
});
