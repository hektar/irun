import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Alert,
  RefreshControl,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import prompt from 'react-native-prompt-android';
import API from '../../config/Api';
import _ from 'lodash';
import Colors from '../../config/Colors';
import NoFriends from '../../404Error/NoFriends';

const MaterialIconHeaderButton = props => (
  // the `props` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...props}
    IconComponent={MaterialIcons}
    iconSize={23}
    color="#FFF"
  />
);

export default class FriendListScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Freunde',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons
          HeaderButtonComponent={props => {
            let badge =
              props.badge > 0 ? (
                <View
                  style={{
                    position: 'absolute',
                    height: 14,
                    width: 14,
                    backgroundColor: 'red',
                    borderRadius: 7,
                    justifyContent: 'center',
                    alignItems: 'center',
                    bottom: -7,
                    right: -7,
                  }}>
                  <Text style={{fontSize: 9, color: 'white'}}>
                    {props.badge}
                  </Text>
                </View>
              ) : null;
            return (
              <TouchableOpacity
                style={{marginHorizontal: 11}}
                onPress={props.onPress}>
                <MaterialIcons
                  name={props.iconName}
                  size={23}
                  color={props.color}
                />
                {badge}
              </TouchableOpacity>
            );
          }}>
          <Item
            title="Freundschaftsanfragen"
            iconName="person"
            color="#FFF"
            badge={navigation.getParam('openFriendRequests')}
            onPress={navigation.getParam('openFriendRequestsScreen')}
          />

          <Item
            title="Freund hinzufügen"
            iconName="person-add"
            color="#FFF"
            onPress={navigation.getParam('addFriend')}
          />
        </HeaderButtons>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');
    this.friendRequests = [];
    this.state = {
      friendList: [],
      refreshing: true,
    };
  }

  componentDidMount() {
    // Für den Header-Button, damit dieser weiß, welche Methode er ausführen muss
    this.props.navigation.setParams({addFriend: this._addFriend});
    this.props.navigation.setParams({
      openFriendRequestsScreen: this._openFriendRequestsScreen,
    });
    this._fetchFriends();
  }

  render() {
    if (this.state.friendList.length == 0 && !this.state.refreshing) {
      return <NoFriends />;
    } else {
      return (
        <FlatList
          data={this.state.friendList}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({refreshing: true}, () => {
                  this._refresh();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          style={styles.container}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          ListHeaderComponent={this._renderHeader}
          keyExtractor={this._keyExtractor}
        />
      );
    }
  }

  _refresh = () => {
    this._fetchFriends();
  };

  _fetchFriends = () => {
    fetch(`${API.BASE_URL}${API.USER_FRIENDS}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw resp.status;
        }
      })
      .then(json => {
        console.log(json);
        if (json) {
          let friends = [];
          json.friendList.forEach(friend => {
            friends.push({
              username: friend.username,
              _id: friend._id,
              requested: false,
            });
          });

          json.sentFriendRequests.forEach(requestedFriend => {
            friends.push({
              username: requestedFriend.username,
              _id: requestedFriend._id,
              requested: true,
            });
          });

          this.friendRequests = json.friendRequests;

          this.props.navigation.setParams({
            openFriendRequests: json.friendRequests.length,
          });

          this.setState({
            friendList: friends,
            refreshing: false,
          });
        }
      })
      .catch(err => console.log(err));
  };

  _addFriend = () => {
    prompt(
      'Freund hinzufügen',
      'Gib den Namen deines Freundes an',
      [
        {
          text: 'Abbrechen',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: friendname => {
            if (friendname == this.user.username) {
              Alert.alert(
                'Nee, das geht nicht!',
                'Anfragen an dich selber sind nicht möglich :)',
                [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                {cancelable: false},
              );
            } else if (_.find(this.state.friendList, {username: friendname})) {
              Alert.alert(
                'Nope!',
                'Dieser User ist bereits in deiner Liste',
                [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                {cancelable: false},
              );
            } else {
              let body = {
                username: friendname,
              };
              fetch(`${API.BASE_URL}${API.USER_ADD_FRIEND}`, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Token ${this.user.token}`,
                },
                body: JSON.stringify(body),
              })
                .then(resp => {
                  console.log(resp);
                  switch (resp.status) {
                    case 200:
                      this._fetchFriends();
                      break;
                    case 404:
                      Alert.alert(
                        'Es tut uns leid!',
                        'Wir haben keinen Nutzer unter diesem Namen gefunden',
                        [
                          {
                            text: 'OK',
                            onPress: () => console.log('OK Pressed'),
                          },
                        ],
                        {cancelable: false},
                      );
                      break;
                  }
                })
                .catch(err => console.log(err));
            }
          },
        },
      ],
      {
        cancelable: false,
        placeholder: 'Benutzername',
      },
    );
  };

  _openFriendRequestsScreen = () => {
    this.props.navigation.navigate('FriendRequests', {
      user: this.user,
      friendRequests: this.friendRequests,
      refreshFriendList: this._refresh,
    });
  };

  _renderItem = ({item}) => {
    let altImg = {
      uri: `https://ui-avatars.com/api/?name=${item.username}&length=1`,
    };
    let imgContainer = <Image style={styles.avatar} source={altImg} />;
    let requested = item.requested ? (
      <Text style={styles.requestedText}>Freundschaftsanfrage steht aus</Text>
    ) : null;
    return (
      <TouchableOpacity
        onPress={() => {
          console.log(item);
          this.props.navigation.navigate('Friend', {
            user: this.user,
            friend: item,
          });
        }}>
        <View style={styles.itemContainer}>
          {imgContainer}
          <View>
            <Text style={styles.username}>{item.username}</Text>
            {requested}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    height: 48,
    width: 48,
    borderRadius: 24,
  },
  itemContainer: {
    flexDirection: 'row',
    padding: 16,
    alignItems: 'center',
  },
  username: {
    marginLeft: 16,
  },
  requestedText: {
    marginLeft: 16,
    fontSize: 8,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
