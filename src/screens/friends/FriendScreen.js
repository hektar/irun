import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import API from '../../config/Api';
import Colors from '../../config/Colors';

export default class FriendScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.getParam('friend').username}'s Profil`,
      headerBackTitle: null,
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');

    this.state = {
      loading: true,
      friend: props.navigation.getParam('friend'),
    };
  }
  componentDidMount() {
    this._fetchFriendInfo();
  }

  render() {
    let altImg = {
      uri: `https://ui-avatars.com/api/?name=${this.state.friend.username}&length=1`,
    };
    return (
      <View style={styles.container}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 16,
            backgroundColor: 'rgba(0,0,0,0.75)',
          }}>
          <Image style={styles.imgProfile} source={altImg} />
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{ color: '#FFF', maxWidth: 256, marginTop: 16, fontSize: 20 }}>
            {this.state.friend.username}'s Profil
          </Text>
        </View>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={() => {
                this.setState({ loading: true }, () => {
                  this._fetchFriendInfo();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          data={this.state.friend.activities}
          ListEmptyComponent={this._renderEmptyActivities}
          renderItem={this._renderItem}
          ListHeaderComponent={this._renderHeader}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
        />
      </View>
    );
  }

  _renderHeader = () => {
    return (
      <View style={styles.itemContainer}>
        <Text style={{ textDecorationLine: 'underline' }}>
          Letzte Aktivitäten
        </Text>
      </View>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };

  _renderEmptyActivities = ({ item, index }) => {
    return (
      <View style={styles.itemContainer}>
        <Text>{this.state.friend.username} hat zurzeit keine Aktivitäten</Text>
      </View>
    );
  };

  _renderItem = ({ item, index }) => {
    console.log(item);
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('FriendStatistic', {
            track: item.track,
          });
        }}
        style={styles.itemContainer}>
        <Text>{item.title}</Text>
      </TouchableOpacity>
    );
  };

  _fetchFriendInfo = () => {
    fetch(
      `${API.BASE_URL}${API.USER_FRIENDS}/${this.state.friend._id}/info`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${this.user.token}`,
        },
      },
    )
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw resp.status;
        }
      })
      .then(json => {
        console.log(json);
        this.setState({
          loading: false,
          friend: json.friend,
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgProfile: {
    height: 128,
    width: 128,
    borderRadius: 64,
    borderWidth: 4,
    borderColor: '#7f8c8d',
    resizeMode: 'cover',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
