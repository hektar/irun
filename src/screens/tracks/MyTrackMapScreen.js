import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Platform,
  StatusBar,
  PermissionsAndroid,
  InteractionManager,
  TouchableOpacity,
  Alert,
  Text,
  BackHandler,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import API from '../../config/Api';
/*
  Cirle rendert die Farben nicht korrekt in iOS
  Bugfix: 
  in react-native-maps/lib/components/MapCirlce hinzufügen 
  componentDidUpdate(props) { this.setNativeProps(props) }
*/
import MapView, {
  PROVIDER_GOOGLE,
  Polyline,
  Circle,
  Marker,
} from 'react-native-maps';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import SignalMeter from '../../components/SignalMeter';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import Colors from '../../config/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {SafeAreaView} from 'react-navigation';
import {HeaderBackButton} from 'react-navigation-stack';
import Stopwatch from '../../components/Stopwatch';
import AwesomeAlert from 'react-native-awesome-alerts';

const MaterialIconHeaderButton = props => (
  <HeaderButton
    {...props}
    IconComponent={MaterialIcons}
    iconSize={23}
    color="#FFF"
  />
);

// Geofenceradius vom Checkpoint in Meter
const GEOFENCE_RADIUS = 10;
// Anzahl an Checkpoints die beim Abarbeiten geprüft werden
const CHECKLIST_SIZE = 3;

export default class MyTrackMapScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Meine Strecken',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={MaterialIconHeaderButton}>
          <Item
            title="Aktuelle Position"
            iconName="gps-fixed"
            onPress={navigation.getParam('goToUserLocation')}
          />
        </HeaderButtons>
      ),
      headerLeft: (
        <HeaderBackButton
          tintColor="white"
          onPress={navigation.getParam('goBackFunc')}
        />
      ),
    };
  };

  constructor(props) {
    super(props);

    this.user = props.navigation.getParam('user');
    this.track = props.navigation.getParam('track');
    this.isFriendTrack = props.navigation.getParam('isFriendTrack', false);

    // Checkpoints von der Strecke
    this.checkpoints = [...this.track.trackProfile];

    // Checkliste für Geofences (Prüfe immer auf die nächsten 3)
    this.checklist = this.checkpoints.slice(1, CHECKLIST_SIZE + 1);

    // Ergebnis mit initalem Startpunkt
    this.checkpointTimes = [
      {
        checkpointId: this.checkpoints[0]._id,
        title: 'Start',
        time: 0,
      },
    ];
    // Indexzeiger auf die Checkpointliste
    this.pointer = CHECKLIST_SIZE;

    this.lastPosition = null;
    this.startTime = null;
    this.endTime = null;

    this.state = {
      inStartPosition: false,
      stopwatchIsRunning: false,
      gpsAccuracy: 0,
      showCheckpoints: false,
      showAlert: false,
    };
  }

  componentDidMount() {
    // Für den Header-Button, damit dieser weiß, welche Methode er ausführen muss
    this.props.navigation.setParams({
      goToUserLocation: this._goToUserLocation,
      goBackFunc: this._goBack,
    });

    BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);

    BackgroundGeolocation.configure({
      locationProvider: BackgroundGeolocation.RAW_PROVIDER,
      distanceFilter: 1,
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      notificationTitle: 'iRun Tracking',
      notificationText: 'iRun nimmt deine Position auf',
      debug: false,
      startOnBoot: false,
      stopOnTerminate: true,
      interval: 1000,
      activityType: 'Fitness',
      stopOnStillActivity: false,
    });

    BackgroundGeolocation.on('location', this._onLocationChanged);

    BackgroundGeolocation.on('error', error => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started');
    });

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped');
    });

    BackgroundGeolocation.on('authorization', status => {
      console.log(
        '[INFO] BackgroundGeolocation authorization status: ' + status,
      );
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(
          () =>
            Alert.alert(
              'App requires location tracking permission',
              'Would you like to open app settings?',
              [
                {
                  text: 'Yes',
                  onPress: () => BackgroundGeolocation.showAppSettings(),
                },
                {
                  text: 'No',
                  onPress: () => console.log('No Pressed'),
                  style: 'cancel',
                },
              ],
            ),
          1000,
        );
      }
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.checkStatus(status => {
      console.log('[INFO] status', status);
      console.log(
        '[INFO] BackgroundGeolocation service is running',
        status.isRunning,
      );
      console.log(
        '[INFO] BackgroundGeolocation services enabled',
        status.locationServicesEnabled,
      );
      console.log(
        '[INFO] BackgroundGeolocation auth status: ' + status.authorization,
      );
    });

    this._startPositionUpdates();
  }

  componentWillUnmount() {
    this._stopPositionUpdates();
    BackgroundGeolocation.removeAllListeners();
    BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#545d5e" barStyle="light-content" />
        <View style={{flex: 9}}>
          <MapView
            ref={ref => (this.map = ref)}
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            onLayout={() => {
              this.map.fitToCoordinates(this.track.trackProfile, {
                edgePadding: {
                  top: 16,
                  right: 16,
                  bottom: 16,
                  left: 16,
                },
                animated: false,
              });
            }}
            showsMyLocationButton={false}
            showsPointsOfInterest={false}
            showsUserLocation={true}
            onMapReady={this._onMapReady}>
            {this._renderPolyline()}
            {this._renderCheckpoints()}
            {this._renderStartPoint()}
          </MapView>
          <View style={{position: 'absolute', right: 0}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{fontSize: 10}}>GPS</Text>
              <SignalMeter strength={this.state.gpsAccuracy} />
            </View>
          </View>
        </View>
        <View style={{flex: 3}}>
          <View style={{alignItems: 'center'}}>
            <Stopwatch ref={ref => (this.stopwatch = ref)} />
            <TouchableOpacity
              underlayColor={
                this.state.stopwatchIsRunning ? '#C60808' : '#A0AC1A'
              }
              style={[
                styles.buttonStart,
                this.state.stopwatchIsRunning && styles.buttonStop,
                // this.state.inStartPosition && styles.buttonDisabled
              ]}
              onPress={this._triggerRun}>
              {this._getIcon()}
            </TouchableOpacity>
          </View>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          progressSize="large"
          title="Lade hoch"
          progressColor={Colors.accent}
          showProgress={true}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={false}
        />
      </SafeAreaView>
    );
  }

  _goBack = () => {
    if (this.state.stopwatchIsRunning) {
      Alert.alert(
        'Lauf beenden?',
        'Du bist noch nicht am Ziel angekommen. Willst du wirklich deinen Lauf beenden?',
        [
          {
            text: 'Beenden',
            onPress: () => {
              this._toggleStopwatch();
              this._resetValues();
              this.props.navigation.goBack();
            },
            style: 'destructive',
          },
          {
            text: 'Weitermachen',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.goBack();
    }
  };

  _triggerRun = () => {
    if (!this.state.stopwatchIsRunning) {
      if (this.state.inStartPosition) {
        this._toggleStopwatch();
      } else {
        Alert.alert(
          'Info',
          'Bitte begib dich zum Startpunkt!',
          [
            {
              text: 'OK',
              onPress: () => {},
            },
          ],
          {cancelable: false},
        );
      }
    } else {
      Alert.alert(
        'Abbrechen?',
        'Du bist noch nicht am Ziel angekommen. Willst du deinen Lauf beenden?',
        [
          {
            text: 'Weitermachen',
            onPress: () => {},
            style: 'cancel',
          },
          {
            text: 'Beenden',
            onPress: () => {
              this._toggleStopwatch();
              this._resetValues();
              this.props.navigation.goBack();
            },
            style: 'destructive',
          },
        ],
        {cancelable: false},
      );
    }
  };

  _handleBackPress = () => {
    this._goBack();
    return true;
  };

  _checkInsideStartpoint = position => {
    let start = this.track.trackProfile[0];

    let distance = this._haversineDistance(
      start.latitude,
      start.longitude,
      position.latitude,
      position.longitude,
    );

    let isInside = distance <= GEOFENCE_RADIUS ? true : false;

    this.setState({
      gpsAccuracy: this._getGPSAccuracyInPercentage(position.accuracy),
      inStartPosition: isInside,
    });
  };

  _startPositionUpdates = () => {
    BackgroundGeolocation.start();
  };

  _checkGeofences = position => {
    console.log('this.checkpoints', this.checkpoints);
    console.log('this.checklist', this.checklist);
    console.log('pointer', this.pointer);

    this.checklist.forEach((cp, idx, cps) => {
      let distance = this._haversineDistance(
        cp.latitude,
        cp.longitude,
        position.latitude,
        position.longitude,
      );

      if (distance <= GEOFENCE_RADIUS) {
        // Falls der Punkt übersprungen wird, läuft diese
        // for-Schleife um die Anzahl verpasster Punkte (also max. CHECKLIST_SIZE mal)
        for (let i = 0; i <= idx; i++) {
          // Erhöhe den Zeiger des Checkpointarrays
          this.pointer++;
          // Lösche das erste Element
          cps.shift();

          // CP getroffen
          if (idx == i) {
            // Füge die Zeit in das Ergebnisarray
            this.checkpointTimes.push({
              checkpointId: cp._id,
              title:
                this.pointer == this.checkpoints.length - 1 + CHECKLIST_SIZE // (this.checkpoints.length - 1) weil der Startpunkt vorweggenommen wurde
                  ? 'Ziel'
                  : `Checkpoint ${cp.distanceFromStart}m`,
              time: this.stopwatch.getTimeInMilliseconds(),
            });
          } else {
            this.checkpointTimes.push({
              checkpointId: cp._id,
              title: 'CP verpasst',
              time: 0, // evtl. mit null füllen
            });
          }

          if (this.pointer < this.checkpoints.length) {
            // Hänge den nächsten Checkpoint dran
            cps.push(this.checkpoints[this.pointer]);
          }
        }

        // Am Ziel angekommen
        if (this.pointer == this.checkpoints.length - 1 + CHECKLIST_SIZE) {
          this._toggleStopwatch();
          Alert.alert(
            'Yeah!!!',
            `Du bist am Ziel angekommen in ${this.stopwatch.getTime()}`,
            [
              {
                text: 'Super!',
                onPress: () => {
                  this._uploadActivity()
                    .then(resp => {
                      console.log(resp);
                      this.setState({
                        showAlert: false,
                      });
                      setTimeout(() => {
                        this.props.navigation.replace('Activity', {
                          user: this.user,
                        });
                      }, 750);
                    })
                    .catch(err => {
                      this.setState({
                        showAlert: false,
                      });
                      console.log(err);
                    });
                },
              },
            ],
            {cancelable: false},
          );
        }
      }
    });
  };

  _uploadActivity = () => {
    this.setState({
      showAlert: true,
    });

    let body = {
      trackId: this.track._id,
      checkpointTimes: this.checkpointTimes,
      isFriendTrack: this.isFriendTrack,
    };

    return fetch(`${API.BASE_URL}${API.ACTIVITIES_ROUTE}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
      body: JSON.stringify(body),
    });
  };

  _stopPositionUpdates = () => {
    BackgroundGeolocation.stop();
  };

  _onLocationChanged = location => {
    console.log('POSITION', location);
    this.lastPosition = location;
    if (!this.state.stopwatchIsRunning) {
      this._checkInsideStartpoint(location);
    } else {
      this._checkGeofences(location);
    }
  };

  _renderCheckpoints = () => {
    if (this.state.showCheckpoints) {
      return this.track.trackProfile.map(p => (
        <Circle
          key={p._id}
          center={{latitude: p.latitude, longitude: p.longitude}}
          radius={GEOFENCE_RADIUS}
          fillColor="rgba(72, 126, 176,0.4)"
          strokeColor="#487eb0"
          strokeWidth={3}
        />
      ));
    }
  };

  _renderPolyline = () => {
    if (this.track.coords.length > 0) {
      return (
        <Polyline
          coordinates={this.track.coords}
          strokeColor="#487eb0"
          strokeWidth={10}
        />
      );
    } else {
      return null;
    }
  };

  _renderStartPoint = () => {
    let start = this.track.trackProfile[0];
    return (
      <Marker
        title="Startpunkt"
        coordinate={{latitude: start.latitude, longitude: start.longitude}}
        pinColor="#0077ff"
      />
    );
  };

  _resetValues = () => {
    this.stopwatch.reset();
    this.endTime = null;
  };

  _toggleStopwatch = () => {
    if (!this.state.stopwatchIsRunning) {
      //Starte Stopuhr
      this.setState(oldState => {
        return {
          stopwatchIsRunning: !oldState.stopwatchIsRunning,
        };
      });
      this.startTime = moment();
      this.stopwatch.start();
    } else {
      //Stoppe Stopuhr
      this.setState(oldState => {
        return {
          stopwatchIsRunning: !oldState.stopwatchIsRunning,
          inStartPosition: false,
        };
      });
      this.endTime = moment();
      this.stopwatch.stop();
    }
  };

  _getIcon = () => {
    return (
      <FontAwesome
        name={this.state.stopwatchIsRunning ? 'stop' : 'play'}
        size={30}
        color="#fff"
      />
    );
  };

  _getFormattedTime = time => {
    this.currentTime = time;
  };

  // Entfernung zweier Koordinaten nach der Haversine Formel in Meter
  // https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
  _haversineDistance = (lat1, lon1, lat2, lon2) => {
    let p = Math.PI / 180; // Math.PI / 180
    let a =
      0.5 -
      Math.cos((lat2 - lat1) * p) / 2 +
      (Math.cos(lat1 * p) *
        Math.cos(lat2 * p) *
        (1 - Math.cos((lon2 - lon1) * p))) /
        2;
    let distance = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    return distance * 1000;
  };

  _goToUserLocation = () => {
    if (this.lastPosition) {
      this.map.animateCamera(
        {
          center: {
            latitude: this.lastPosition.latitude,
            longitude: this.lastPosition.longitude,
          },
          pitch: 10,
          zoom: 17,
        },
        {duration: 1500},
      );
    }
  };

  _onMapReady = () => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(granted => {
        console.log('granted: ', granted);
      });
    }
    this.setState({showCheckpoints: true});
  };

  _getGPSAccuracyInPercentage = accuracy => {
    let accuracyInPercent = (5 / accuracy) * 100;
    if (accuracyInPercent < 1) {
      accuracyInPercent = 1;
    }
    return accuracyInPercent;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6fa',
  },
  map: {
    width: '100%',
    height: '100%',
  },
  buttonStart: {
    marginTop: 16,
    marginBottom: 16,
    alignSelf: 'center',
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: '#CDDC39',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonDisabled: {
    backgroundColor: '#aaa',
  },
  buttonStop: {
    backgroundColor: '#FF0000',
  },
});
