import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import Colors from '../../config/Colors';
import API from '../../config/Api';
import NoTracks from '../../404Error/NoTracks';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import IRunFont from '../../components/IRunFont';

const IRunIconHeaderButton = props => (
  // the `props` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...props}
    IconComponent={IRunFont}
    iconSize={23}
    color="#FFF"
  />
);

export default class MyTracksScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Meine Strecken',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={IRunIconHeaderButton}>
          <Item
            title="Strecken von Freunden"
            iconName="gender_neutral_user"
            onPress={navigation.getParam('openFriendTracks')}
          />
        </HeaderButtons>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');
    this.state = {
      refreshing: true,
      tracks: [],
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      openFriendTracks: this._openFriendTracks,
    });
    this._fetchMyTracks();
  }

  render() {
    if (this.state.tracks.length == 0 && !this.state.refreshing) {
      return <NoTracks />;
    } else {
      return (
        <FlatList
          data={this.state.tracks}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({refreshing: true}, () => {
                  this._fetchMyTracks();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          style={styles.container}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
        />
      );
    }
  }

  _openFriendTracks = () => {
    this.props.navigation.navigate('TracksOfFriends', {
      user: this.user,
      userTrack: this.track,
      trackIntention: true,
    });
  };

  _fetchMyTracks = () => {
    fetch(`${API.BASE_URL}${API.TRACKS_ROUTE}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw `Verbindungsfehler Status ${resp.status}`;
        }
      })
      .then(tracks => {
        console.log(tracks);
        if (tracks) {
          this.setState({tracks: tracks, refreshing: false});
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({refreshing: false});
      });
  };

  _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('MyTrackMap', {
            user: this.user,
            track: item,
          });
        }}>
        <View style={styles.itemContainer}>
          <View>
            <Text>{item.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
