import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import API from '../../config/Api';
import {
  AreaChart,
  Grid,
  ProgressCircle,
  XAxis,
  YAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import Colors from '../../config/Colors';
import AnimateNumber from 'react-native-animate-number';
import IRunFont from '../../components/IRunFont';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import AwesomeAlert from 'react-native-awesome-alerts';

const IRunIconHeaderButton = props => (
  // the `props` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...props}
    IconComponent={IRunFont}
    iconSize={23}
    color="#FFF"
  />
);

export default class TrackCompareScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Ergebnis Vergleich',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={IRunIconHeaderButton}>
          <Item
            title="Strecke kürzen"
            show={
              navigation.getParam('isSameTrack', false) ? 'never' : 'always'
            }
            iconName="cut"
            onPress={navigation.getParam('cutTrackAndCompare')}
          />
        </HeaderButtons>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');

    let userTrack = props.navigation.getParam('userTrack');
    let friendTrack = props.navigation.getParam('friendTrack');

    this.compareTotalDistance = true;
    this.friendTrackUsername = friendTrack.user.username;

    console.log(userTrack);
    console.log(friendTrack);

    this.state = {
      userTrackY: [],
      userTrackX: [],
      friendTrackY: [],
      friendTrackX: [],
      checkpointTimesUser: [],
      showAlert: true,
      userTrack: userTrack,
      friendTrack: friendTrack,
      trackSimilarity: 0,
      trackSimilarityPercent: 0,
    };
  }

  componentDidMount() {
    this._fetchCompare();
    this.props.navigation.setParams({
      isSameTrack: this.state.userTrack.originTrack
        ? this.state.userTrack.originTrack == this.state.friendTrack._id
        : false,
      cutTrackAndCompare: this._cutTrackAndCompare,
    });
  }

  render() {
    const xAxisData =
      this.state.userTrack.totalDistance >= this.state.friendTrack.totalDistance
        ? this.state.userTrackX
        : this.state.friendTrackX;
    const userColor = 'rgba(134, 65, 244, 0.5)';
    const friendColor = 'rgba(34, 128, 176, 0.5)';
    const minY =
      Math.floor(
        Math.min(
          this.state.userTrack.minElevation,
          this.state.friendTrack.minElevation,
        ) / 10,
      ) * 10;
    const maxY =
      Math.ceil(
        Math.max(
          this.state.userTrack.maxElevation,
          this.state.friendTrack.maxElevation,
        ) / 10,
      ) * 10;
    return (
      <View style={styles.container}>
        <View>
          <View style={{height: 200, padding: 10, flexDirection: 'row'}}>
            <YAxis
              data={this.state.userTrackY}
              min={minY}
              max={maxY}
              // style={{marginBottom: 30}}
              svg={{fontSize: 10, fill: 'grey'}}
              numberOfTicks={5}
              contentInset={{top: 10, bottom: 10}}
            />
            <View style={{flex: 1, marginLeft: 10}}>
              <AreaChart
                style={{flex: 1}}
                data={this.state.userTrackY}
                svg={{fill: userColor}}
                contentInset={{top: 10, bottom: 10}}
                yMin={minY}
                yMax={maxY}
                curve={shape.curveLinear}>
                <Grid />
              </AreaChart>
              <AreaChart
                style={StyleSheet.absoluteFill}
                data={this.state.friendTrackY}
                yMin={minY}
                yMax={maxY}
                svg={{fill: friendColor}}
                contentInset={{top: 10, bottom: 10}}
                curve={shape.curveLinear}
              />
            </View>
          </View>
          <XAxis
            data={xAxisData}
            xAccessor={({index}) => {
              let maxVal = xAxisData.length < 5 ? xAxisData.length : 5;
              let delta = Math.floor(xAxisData.length / maxVal);
              return index % delta == 0
                ? Math.round(xAxisData[index]) / 1000
                : null;
            }}
            // formatLabel={(value, index) => {
            //   let x = Math.round(xAxisData[index]) / 1000;
            //   return x;
            // }}
            contentInset={{left: 33, right: 20}}
            svg={{fontSize: 10, fill: 'grey'}}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}>
          <View style={{width: 12, height: 12, backgroundColor: userColor}} />
          <Text style={{maxWidth: 100}} numberOfLines={1} ellipsizeMode="tail">
            Meine Strecke
          </Text>
          <View style={{width: 12, height: 12, backgroundColor: friendColor}} />
          <Text style={{maxWidth: 100}} numberOfLines={1} ellipsizeMode="tail">
            {this.friendTrackUsername}'s Strecke
          </Text>
        </View>
        <View
          style={{
            height: 128,
            width: '100%',
            flexDirection: 'row',
            marginBottom: 8,
          }}>
          {/* Linker Container */}
          <View style={{flex: 1}}>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont name="road" color={Colors.primaryLight} size={20} />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {(this.state.userTrack.totalDistance / 1000).toFixed(2)} km
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {(this.state.friendTrack.totalDistance / 1000).toFixed(2)}{' '}
                    km
                  </Text>
                </View>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="average_math"
                  color={Colors.primaryLight}
                  size={20}
                />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {this.state.userTrack.elevationDifference} m
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {this.state.friendTrack.elevationDifference} m
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont
                  name="bullish"
                  color={Colors.primaryLight}
                  size={20}
                />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {this.state.userTrack.ascent} m
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {this.state.friendTrack.ascent} m
                  </Text>
                </View>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="bearish"
                  color={Colors.primaryLight}
                  size={20}
                />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {this.state.userTrack.descent} m
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {this.state.friendTrack.descent} m
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont
                  name="double_up"
                  color={Colors.primaryLight}
                  size={20}
                />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {this.state.userTrack.maxElevation} m
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {this.state.friendTrack.maxElevation} m
                  </Text>
                </View>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="double_down"
                  color={Colors.primaryLight}
                  size={20}
                />
                <View>
                  <Text style={[styles.iconText, {color: userColor}]}>
                    {this.state.userTrack.minElevation} m
                  </Text>
                  <Text style={[styles.iconText, {color: friendColor}]}>
                    {this.state.friendTrack.minElevation} m
                  </Text>
                </View>
              </View>
            </View>
          </View>
          {/* Ende linker Container */}
          {/* Rechter Container */}
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <View>
              <ProgressCircle
                style={{height: 100, width: 100}}
                progress={this.state.trackSimilarity}
                progressColor={Colors.accent}
              />
              <View
                style={{
                  width: 64,
                  height: 64,
                  top: 18,
                  left: 18,
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'absolute',
                }}>
                <AnimateNumber
                  style={{
                    fontSize: 22,
                  }}
                  value={this.state.trackSimilarityPercent}
                  countBy={1}
                  timing={(interval, progress) => {
                    // langsamer Start, langsames Ende
                    return interval * (1 - Math.sin(Math.PI * progress)) * 10;
                  }}
                  formatter={val => {
                    return `${val} %`;
                  }}
                />
              </View>
            </View>
          </View>
        </View>
        <FlatList
          style={{
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor: '#ccc',
          }}
          data={this.state.checkpointTimesUser}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
        />
        <AwesomeAlert
          show={this.state.showAlert}
          progressSize="large"
          title="Lade..."
          progressColor={Colors.accent}
          showProgress={true}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={false}
        />
      </View>
    );
  }

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return `${index}`;
  };

  _pad = n => (n < 10 ? '0' + n : n);

  _renderItem = ({item, index}) => {
    const userColor = 'rgba(134, 65, 244, 0.5)';
    const friendColor = 'rgba(34, 128, 176, 0.5)';

    const durationUser = moment.duration(item.time);
    const centisecondsUser = Math.floor(durationUser.milliseconds() / 10);
    const timeStringUser = `${this._pad(durationUser.minutes())}:${this._pad(
      durationUser.seconds(),
    )},${this._pad(centisecondsUser)}`;
    // console.log(this.state.friendTrack);
    const friendItem = this.state.friendTrack.cuttedTimes
      ? this.state.friendTrack.cuttedTimes[index]
      : this.state.friendTrack.activities[0].checkpointTimes[index];
    // console.log(friendItem);
    const durationFriend = moment.duration(friendItem.time);
    const centisecondsFriend = Math.floor(durationFriend.milliseconds() / 10);
    const timeStringFriend = `${this._pad(
      durationFriend.minutes(),
    )}:${this._pad(durationFriend.seconds())},${this._pad(centisecondsFriend)}`;

    const cpDiff = moment(item.time).diff(moment(friendItem.time));
    const durationDiff = moment.duration(cpDiff);

    let isNegative = false;
    if (durationDiff.milliseconds() < 0 || durationDiff.seconds() < 0) {
      isNegative = true;
    }

    const centiseconds2 = isNegative
      ? Math.abs(Math.floor(durationDiff.milliseconds() / 10))
      : Math.floor(durationDiff.milliseconds() / 10);

    const diffTimeString = `${isNegative ? '-' : '+'} ${this._pad(
      Math.abs(durationDiff.minutes()),
    )}:${this._pad(Math.abs(durationDiff.seconds()))},${this._pad(
      centiseconds2,
    )}`;

    return (
      <View style={styles.itemContainer}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <IRunFont name="place_marker" color={Colors.primaryLight} size={20} />
          <Text style={{marginLeft: 16}}>{item.title}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={{color: userColor}}>{timeStringUser}</Text>
            <Text style={{color: friendColor}}>{timeStringFriend}</Text>
          </View>
          <View
            style={{
              marginLeft: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={[
                {alignSelf: 'flex-end', color: 'red'},
                isNegative && {color: 'green'},
              ]}>
              {diffTimeString}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  _determinAxis = (userTrack, friendTrack) => {
    let userTrackY = [];
    let userTrackX = [];
    let friendTrackY = [];
    let friendTrackX = [];

    // Ermittle die Länge vom Array
    let arraySize = 0;
    if (this.compareTotalDistance) {
      arraySize = Math.max(
        userTrack.trackProfile.length,
        friendTrack.trackProfile.length,
      );
    } else {
      arraySize = Math.min(
        userTrack.trackProfile.length,
        friendTrack.trackProfile.length,
      );
    }

    // Stocke bei unterschiedlichen Arraylängen die fehlenden Werte mit null auf
    for (let index = 0; index < arraySize; index++) {
      if (index < userTrack.trackProfile.length) {
        userTrackY.push(userTrack.trackProfile[index].elevation);
        userTrackX.push(userTrack.trackProfile[index].distanceFromStart);
      } else {
        userTrackY.push(null);
        userTrackX.push(null);
      }

      if (index < friendTrack.trackProfile.length) {
        friendTrackY.push(friendTrack.trackProfile[index].elevation);
        friendTrackX.push(friendTrack.trackProfile[index].distanceFromStart);
      } else {
        friendTrackX.push(null);
        friendTrackY.push(null);
      }
    }

    console.log(userTrackX);
    console.log(userTrackY);
    console.log(friendTrackX);
    console.log(friendTrackY);

    this.setState({
      userTrackX: userTrackX,
      userTrackY: userTrackY,
      friendTrackX: friendTrackX,
      friendTrackY: friendTrackY,
    });
  };

  _cutTrackAndCompare = () => {
    this.setState(
      {
        showAlert: true,
      },
      () => {
        this.compareTotalDistance = !this.compareTotalDistance;
        this._fetchCompare();
      },
    );
  };

  _fetchCompare = () => {
    fetch(
      `${API.BASE_URL}${API.TRACKS_COMPARE}?t1=${this.state.userTrack._id}&t2=${this.state.friendTrack._id}&total=${this.compareTotalDistance}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${this.user.token}`,
        },
      },
    )
      .then(resp => {
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw resp.status;
        }
      })
      .then(json => {
        console.log(json);
        let checkpointTimesUser = [];
        // Wenn die Strecken indentisch sind
        if (
          this.state.userTrack.originTrack &&
          this.state.userTrack.originTrack == this.state.friendTrack._id
        ) {
          checkpointTimesUser = json.track1.activities[0].checkpointTimes;
        }
        // Wenn die Strecke geschnitten wurde
        if (!this.compareTotalDistance) {
          checkpointTimesUser = json.track1.cuttedTimes
            ? json.track1.cuttedTimes
            : json.track1.activities[0].checkpointTimes;
        }
        this._determinAxis(json.track1, json.track2);
        this.setState({
          showAlert: false,
          userTrack: json.track1,
          friendTrack: json.track2,
          trackSimilarity: json.similarity,
          checkpointTimesUser: checkpointTimesUser,
          // ParseInt behebt hier einen Fehler in der Darstellung
          trackSimilarityPercent: parseInt(json.similarity * 100, 10),
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          showAlert: false,
        });
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 4,
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  iconRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconText: {
    color: Colors.primaryLight,
    marginLeft: 8,
  },
});
