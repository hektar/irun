import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  RefreshControl,
  SectionList,
  TouchableOpacity,
} from 'react-native';
import API from '../../config/Api';
import Colors from '../../config/Colors';
import _ from 'lodash';
import IRunFont from '../../components/IRunFont';

export default class TracksOfFriendsScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Strecken deiner Freunde',
      headerBackTitle: null,
    };
  };
  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');
    this.trackIntention = props.navigation.getParam('trackIntention', false);
    // Wird hier in der Screen nicht benötigt, nur weitergereicht
    this.userTrack = props.navigation.getParam('userTrack');
    this.state = {
      refreshing: true,
      tracks: [],
    };
  }

  componentDidMount() {
    this._fetchFriendsTracks();
  }
  componentWillUnmount() {}

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          sections={this.state.tracks}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({refreshing: true}, () => {
                  this._fetchFriendsTracks();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          renderSectionHeader={this._renderSectionHeader}
        />
      </View>
    );
  }

  _renderSectionHeader = ({
    section: {
      friend: {username},
    },
  }) => {
    return (
      <View style={styles.header}>
        <IRunFont name="gender_neutral_user" color="#fff" size={25} />
        <Text style={{fontSize: 20, marginLeft: 8, color: '#fff'}}>
          {username}
        </Text>
      </View>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          if (this.trackIntention) {
            this.props.navigation.navigate('MyTrackMap', {
              user: this.user,
              track: item,
              isFriendTrack: this.trackIntention,
            });
          } else {
            this.props.navigation.replace('TrackCompare', {
              user: this.user,
              userTrack: this.userTrack,
              friendTrack: item,
            });
          }
        }}>
        <View style={styles.itemContainer}>
          <View>
            <Text>{item.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _keyExtractor = item => {
    return item._id;
  };

  _fetchFriendsTracks = () => {
    fetch(`${API.BASE_URL}${API.TRACKS_FRIENDS}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
    })
      .then(resp => {
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw resp.status;
        }
      })
      .then(tracksOfFriends => {
        let intermediateResult = {};
        // Bereite die Daten für die SectionList im assoziativen Array vor
        tracksOfFriends.forEach(track => {
          intermediateResult[track.user._id] =
            intermediateResult[track.user._id] || {};
          intermediateResult[track.user._id]['friend'] = track.user;
          intermediateResult[track.user._id]['data'] =
            intermediateResult[track.user._id]['data'] || [];
          intermediateResult[track.user._id]['data'].push(track);
        });
        console.log(intermediateResult);
        // Wandle assoziatives in normales Array um
        let data = [];
        for (const key in intermediateResult) {
          data.push(intermediateResult[key]);
        }

        console.log(data);
        this.setState({
          refreshing: false,
          tracks: data,
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({refreshing: false});
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.accent,
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
