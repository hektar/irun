import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Platform,
  View,
  PermissionsAndroid,
  StatusBar,
  Text,
  Alert,
  InteractionManager,
  BackHandler,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Polyline} from 'react-native-maps';
import {SafeAreaView} from 'react-navigation';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import {HeaderBackButton} from 'react-navigation-stack';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Stopwatch from '../../components/Stopwatch';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import API from '../../config/Api';
import Colors from '../../config/Colors';
import SignalMeter from '../../components/SignalMeter';
import AwesomeAlert from 'react-native-awesome-alerts';

const MaterialIconHeaderButton = props => (
  <HeaderButton
    {...props}
    IconComponent={MaterialIcons}
    iconSize={23}
    color="#FFF"
  />
);

export default class TrackingScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Track',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={MaterialIconHeaderButton}>
          <Item
            title="Aktuelle Position"
            iconName="gps-fixed"
            onPress={navigation.getParam('goToUserLocation')}
          />
        </HeaderButtons>
      ),
      headerLeft: (
        <HeaderBackButton
          tintColor="white"
          onPress={navigation.getParam('goBackFunc')}
        />
      ),
    };
  };

  constructor(props) {
    super(props);

    this.user = props.navigation.getParam('user');

    this.trackedPositionData = [];
    this.lastPosition = null;
    this.startTime = null;
    this.endTime = null;

    this.state = {
      stopwatchIsRunning: false,
      polylineCoords: [],
      gpsAccuracy: 0,
      showAlert: false,
    };
  }

  componentDidMount() {
    // Für den Header-Button, damit dieser weiß, welche Methode er ausführen muss
    this.props.navigation.setParams({
      goToUserLocation: this._goToUserLocation,
      goBackFunc: this._goBack,
      openDrawer: this._openDrawer,
    });

    BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);

    BackgroundGeolocation.configure({
      locationProvider: BackgroundGeolocation.RAW_PROVIDER,
      distanceFilter: 1,
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      notificationTitle: 'iRun Tracking',
      notificationText: 'iRun nimmt deine Position auf',
      debug: false,
      startOnBoot: false,
      stopOnTerminate: true,
      interval: 1000,
      activityType: 'Fitness',
      stopOnStillActivity: false,
    });

    BackgroundGeolocation.on('location', this._onLocationChanged);

    BackgroundGeolocation.on('error', error => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started');
    });

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped');
    });

    BackgroundGeolocation.on('authorization', status => {
      console.log(
        '[INFO] BackgroundGeolocation authorization status: ' + status,
      );
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(
          () =>
            Alert.alert(
              'App requires location tracking permission',
              'Would you like to open app settings?',
              [
                {
                  text: 'Yes',
                  onPress: () => BackgroundGeolocation.showAppSettings(),
                },
                {
                  text: 'No',
                  onPress: () => console.log('No Pressed'),
                  style: 'cancel',
                },
              ],
            ),
          1000,
        );
      }
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.checkStatus(status => {
      console.log('[INFO] checkStatus', status);
      console.log(
        '[INFO] BackgroundGeolocation service is running',
        status.isRunning,
      );
      console.log(
        '[INFO] BackgroundGeolocation services enabled',
        status.locationServicesEnabled,
      );
      console.log(
        '[INFO] BackgroundGeolocation auth status: ' + status.authorization,
      );
    });

    setTimeout(() => {
      this._goToUserLocation();
    }, 1000);
  }

  componentWillUnmount() {
    // unregister all event listeners
    BackgroundGeolocation.removeAllListeners();
    BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{flex: 9}}>
          <MapView
            ref={ref => (this.map = ref)}
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            showsMyLocationButton={false}
            showsPointsOfInterest={false}
            showsUserLocation={true}
            onMapReady={this._onMapReady}>
            {this._renderTrail()}
          </MapView>
          <View style={{position: 'absolute', right: 0}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{fontSize: 10}}>GPS</Text>
              <SignalMeter strength={this.state.gpsAccuracy} />
            </View>
          </View>
        </View>
        <View style={{flex: 3}}>
          <View style={{alignItems: 'center'}}>
            <Stopwatch ref={ref => (this.stopwatch = ref)} />
            <TouchableOpacity
              underlayColor={
                this.state.stopwatchIsRunning ? '#C60808' : '#A0AC1A'
              }
              style={[
                styles.buttonStart,
                this.state.stopwatchIsRunning && styles.buttonStop,
                // this.state.startBtnDisabled && styles.buttonDisabled
              ]}
              onPress={this._toggleStopwatch}>
              {this._getIcon()}
            </TouchableOpacity>
          </View>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          progressSize="large"
          title="Lade hoch"
          progressColor={Colors.accent}
          showProgress={true}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={false}
        />
      </SafeAreaView>
    );
  }

  // ---------------------------------------------------------------------

  _goBack = () => {
    if (this.state.stopwatchIsRunning) {
      Alert.alert(
        'Lauf beenden?',
        'Du bist noch aktiv am Aufzeichen. Alle Daten gehen verloren!',
        [
          {
            text: 'Lauf beenden',
            onPress: () => {
              BackgroundGeolocation.stop();
              this.props.navigation.goBack();
            },
            style: 'destructive',
          },
          {
            text: 'Weitermachen',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.goBack();
    }
  };

  _handleBackPress = () => {
    this._goBack();
    return true;
  };

  _showAlert = () => {
    this.setState({
      showAlert: true,
    });
  };

  _hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  _renderTrail = () => {
    if (this.state.polylineCoords.length > 0) {
      return (
        <Polyline
          coordinates={this.state.polylineCoords}
          strokeColor="#487eb0"
          strokeWidth={10}
        />
      );
    } else {
      return null;
    }
  };

  _trackPositionData = location => {
    if (this.lastPosition == null) {
      this.lastPosition = location;
    }

    this.trackedPositionData.push({
      latitude: location.latitude,
      longitude: location.longitude,
      timestamp: Date.now(),
      time: location.time,
      speed: location.speed,
    });

    // Polyline-Daten werden in den State gespeiechert und gerendert
    this.setState(oldState => {
      const newPolylineCoords = oldState.polylineCoords.concat({
        latitude: location.latitude,
        longitude: location.longitude,
      });

      return {
        polylineCoords: newPolylineCoords,
        gpsAccuracy: this._getGPSAccuracyInPercentage(location.accuracy),
      };
    });
  };

  _onLocationChanged = location => {
    console.log('POSITION', location);
    location.time = this.stopwatch.getTimeInMilliseconds();
    this._trackPositionData(location);
    this.lastPosition = location;
  };

  _startPositionUpdates = () => {
    // Existierende Polylinie löschen
    if (this.state.polylineCoords.length > 0) {
      this.setState({polylineCoords: []});
    }
    BackgroundGeolocation.start();
  };

  _uploadTrackedData = trackedData => {
    console.log(trackedData);

    this._showAlert();

    let track = {
      title: 'Strecke vom ' + this.endTime.format('DD.MM.YYYY HH:mm'),
      userId: this.user._id,
      coords: trackedData,
    };

    // let processedTrack = this._processTrack(trackedData);

    fetch(`${API.BASE_URL}${API.TRACKS_ROUTE}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
      body: JSON.stringify(track),
    })
      .then(resp => {
        if (resp.status == 200) {
          return null;
        } else if (resp.status == 400) {
          return resp.json();
        }
      })
      .then(json => {
        console.log(json);
        this._hideAlert();
        if (json == null) {
          setTimeout(() => {
            this.props.navigation.replace('Activity', {
              user: this.user,
            });
          }, 750);
        } else if (json.error) {
          setTimeout(() => {
            Alert.alert(
              'Strecke nicht gespeichert',
              `${json.error.msg}`,
              [
                {
                  text: 'OK',
                  onPress: () => {},
                },
              ],
              {cancelable: false},
            );
          }, 750);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  _stopPositionUpdates = () => {
    BackgroundGeolocation.stop();
    Alert.alert(
      'Aktivität beendet',
      'Strecke hochladen?',
      [
        {
          text: 'Hochladen',
          onPress: () => {
            this._uploadTrackedData(this.trackedPositionData);
            this._resetValues();
          },
        },
        {
          text: 'Abbrechen',
          onPress: () => {
            this._resetValues();
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  _resetValues = () => {
    this.setState({polylineCoords: []});
    this.trackedPositionData = [];
    this.stopwatch.reset();
    this.lastPosition = null;
  };

  _toggleStopwatch = () => {
    if (!this.state.stopwatchIsRunning) {
      //Starte Stopuhr
      this.setState(oldState => {
        return {
          stopwatchIsRunning: !oldState.stopwatchIsRunning,
        };
      });
      this.startTime = moment();
      this.stopwatch.start();
      this._startPositionUpdates();
    } else {
      //Stoppe Stopuhr
      this.setState(oldState => {
        return {
          stopwatchIsRunning: !oldState.stopwatchIsRunning,
        };
      });
      this.endTime = moment();
      this.stopwatch.stop();
      this._stopPositionUpdates();
    }
  };

  _getIcon = () => {
    return (
      <FontAwesome
        name={this.state.stopwatchIsRunning ? 'stop' : 'play'}
        size={30}
        color="#fff"
      />
    );
  };

  _getFormattedTime = time => {
    this.currentTime = time;
  };

  _onMapReady = () => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(granted => {
        console.log('granted: ', granted);
      });
    }
  };

  _goToUserLocation = () => {
    console.log(this.lastPosition);

    if (!this.state.stopwatchIsRunning) {
      BackgroundGeolocation.getCurrentLocation(
        location => {
          console.log(location);
          this.map.animateCamera(
            {
              center: {
                latitude: location.latitude,
                longitude: location.longitude,
              },
              pitch: 10,
              zoom: 17,
            },
            {duration: 1500},
          );
          this.setState({
            gpsAccuracy: this._getGPSAccuracyInPercentage(location.accuracy),
          });
        },
        error => {
          console.log(error.code, error.message);
          switch (error.code) {
            case 1:
              alert('Keine Berechtigung für Standorterfassung erhalten');
              break;
            case 2:
              alert('Keinen Standort ermittelt');
              break;
            case 3:
              alert('Zeitüberschreitung bei der Standorterfassung');
              break;
          }
          alert(error.message);
        },
        {
          maximumAge: 10000,
          enableHighAccuracy: true,
          timeout: 5000,
        },
      );
    } else {
      if (this.lastPosition) {
        this.map.animateCamera(
          {
            center: {
              latitude: this.lastPosition.latitude,
              longitude: this.lastPosition.longitude,
            },
            pitch: 10,
            zoom: 17,
          },
          {duration: 1500},
        );
      }
    }
  };

  _openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  _getGPSAccuracyInPercentage = accuracy => {
    let accuracyInPercent = (5 / accuracy) * 100;
    if (accuracyInPercent < 1) {
      accuracyInPercent = 1;
    }
    return accuracyInPercent;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6fa',
  },
  buttonStart: {
    marginTop: 16,
    marginBottom: 16,
    alignSelf: 'center',
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: '#CDDC39',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStop: {
    backgroundColor: '#FF0000',
  },
  map: {
    width: '100%',
    height: '100%',
  },
});
