import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    RefreshControl,
    TouchableOpacity,
} from 'react-native';
import Colors from '../../config/Colors';
import API from '../../config/Api';

export default class RankingTracksScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Rangliste',
            headerBackTitle: null,
        };
    };

    constructor(props) {
        super(props);
        this.user = props.navigation.getParam('user');
        this.state = {
            refreshing: true,
            tracks: [],
        };
    }

    componentDidMount() {
        this._fetchMyTracks();
    }

    render() {
        return (
            <FlatList
                data={this.state.tracks}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={() => {
                            this.setState({ refreshing: true }, () => {
                                this._fetchMyTracks();
                            });
                        }}
                        colors={[Colors.accent]}
                        title="Aktualisiere"
                        tintColor={Colors.accent}
                        titleColor={Colors.accent}
                    />
                }
                style={styles.container}
                renderItem={this._renderItem}
                ItemSeparatorComponent={this._renderSeparator}
                keyExtractor={this._keyExtractor}
            />
        );
    }

    _fetchMyTracks = () => {
        fetch(`${API.BASE_URL}${API.TRACKS_ROUTE}?bestTime=true`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Token ${this.user.token}`,
            },
        })
            .then(resp => {
                console.log(resp);
                if (resp.status == 200) {
                    return resp.json();
                } else {
                    throw `Verbindungsfehler Status ${resp.status}`
                }
            })
            .then(tracks => {
                console.log(tracks);
                if (tracks) {
                    this.setState({ tracks: tracks, refreshing: false });
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({ refreshing: false });
            });
    };

    _renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.navigate('Ranking', {
                        user: this.user,
                        track: item,
                    });
                }}>
                <View style={styles.itemContainer}>
                    <View>
                        <Text>{item.title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    _renderSeparator = () => {
        return <View style={styles.separator} />;
    };

    _keyExtractor = (item, index) => {
        return item._id;
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemContainer: {
        height: 56,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#ccc',
    },
});