import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, RefreshControl } from 'react-native';
import API from '../../config/Api';
import Colors from '../../config/Colors';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import _ from 'lodash';

export default class RankingScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Rangliste',
      headerBackTitle: null,
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');
    this.track = props.navigation.getParam('track');

    this.state = {
      refreshing: true,
      rankings: [],
    };
  }

  componentDidMount() {
    this._fetchRankings();
  }
  componentWillUnmount() { }

  render() {
    return (
      <FlatList
        data={this.state.rankings}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => {
              this.setState({ refreshing: true }, () => {
                this._fetchRankings();
              });
            }}
            colors={[Colors.accent]}
            title="Aktualisiere"
            tintColor={Colors.accent}
            titleColor={Colors.accent}
          />
        }
        style={styles.container}
        renderItem={this._renderItem}
        ItemSeparatorComponent={this._renderSeparator}
        keyExtractor={this._keyExtractor}
      />
    );
  }

  _pad = n => (n < 10 ? '0' + n : n);

  _formatTime = time => {
    const duration = moment.duration(time);
    const centiseconds = Math.floor(duration.milliseconds() / 10);
    return `${this._pad(duration.minutes())}:${this._pad(
      duration.seconds(),
    )},${this._pad(centiseconds)}`;
  };

  _renderItem = ({ item, index }) => {
    return (
      <View style={styles.itemContainer}>
        <View
          style={{
            flex: 2,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={[
              {
                height: 24,
                width: 24,
                borderRadius: 12,
                alignItems: 'center',
                justifyContent: 'center',
              },
              index == 0 && styles.first,
              index == 1 && styles.second,
              index == 2 && styles.third,
            ]}>
            <Text>{index + 1}</Text>
          </View>
        </View>
        <View
          style={{
            flex: 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{item.name}</Text>
        </View>
        <View
          style={{
            flex: 7,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{this._formatTime(item.time)}</Text>
        </View>
      </View>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return `${index}`;
  };

  _fetchRankings = () => {
    fetch(`${API.BASE_URL}${API.RANKINGS_ROUTE}?id=${this.track._id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw `Verbindungsfehler Status ${resp.status}`;
        }
      })
      .then(rankings => {
        console.log(rankings);

        // Verarbeite das Ergebnis in ein Array
        let rankingsResult = [];
        for (const friendName in rankings) {
          if (rankings.hasOwnProperty(friendName)) {
            console.log(rankings[friendName]);
            rankingsResult.push({
              name: friendName,
              time: rankings[friendName].finishTime
            });
          }
        }
        // Füge den User hinzu und sortiere anschließend
        rankingsResult.push({
          name: this.user.username,
          time: this.track.bestTime,
        });

        console.log(rankingsResult);


        this.setState({
          refreshing: false,
          rankings: _.orderBy(
            rankingsResult,
            elem => {
              return elem.time;
            },
            'asc',
          ),
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  first: {
    backgroundColor: 'gold',
  },
  second: {
    backgroundColor: 'silver',
  },
  third: {
    backgroundColor: 'rgb(205, 127, 50)',
  },
  itemContainer: {
    height: 56,
    flexDirection: 'row',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
