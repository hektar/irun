import React, {Component} from 'react';
import {
  View,
  Image,
  Alert,
  StatusBar,
  Animated,
  Dimensions,
  Easing,
  ImageBackground,
  TouchableOpacity,
  Text,
  TextInput,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../../config/Colors';
import Keys from '../../config/StorageKeys';
import API from '../../config/Api';
import AsyncStorage from '@react-native-community/async-storage';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 52;

export default class LoginScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      passwordRepeat: '',
      loading: false,
    };
    this.spinValue = new Animated.Value(0);
    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
  }

  _onGrow(callback) {
    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(callback);
  }

  _onChangeUsername = textInput => {
    this.setState({username: textInput});
  };

  _onChangePassword = textInput => {
    this.setState({password: textInput});
  };

  _onChangePasswordRepeat = textInput => {
    this.setState({passwordRepeat: textInput});
  };

  _onRegister = async () => {
    Keyboard.dismiss();

    if (this.state.loading) return;

    this.setState({loading: true});

    Animated.timing(this.buttonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();

    Animated.loop(
      Animated.timing(this.spinValue, {
        toValue: 1,
        duration: 2500,
        easing: Easing.linear,
      }),
    ).start();

    if (this.state.username == '') {
      Alert.alert(
        'Ungültige Eingabe',
        'Bitte gib einen Benutzernamen ein',
        [
          {
            text: 'OK',
            onPress: () => {
              this.setState({loading: false});
              Animated.timing(this.buttonAnimated, {
                toValue: 0,
                duration: 200,
                easing: Easing.linear,
              }).start();
              this.growAnimated.setValue(0);
              this.spinValue.setValue(0);
            },
          },
        ],
        {cancelable: false},
      );
      return;
    }

    if (
      this.state.password == '' ||
      this.state.passwordRepeat == '' ||
      this.state.password != this.state.passwordRepeat
    ) {
      Alert.alert(
        'Ungültige Eingabe',
        'Dein Passwort stimmt nicht überein',
        [
          {
            text: 'OK',
            onPress: () => {
              this.setState({loading: false});
              Animated.timing(this.buttonAnimated, {
                toValue: 0,
                duration: 200,
                easing: Easing.linear,
              }).start();
              this.growAnimated.setValue(0);
              this.spinValue.setValue(0);
            },
          },
        ],
        {cancelable: false},
      );
      return;
    }

    let body = {
      username: this.state.username,
      password: this.state.password,
    };

    fetch(`${API.BASE_URL}${API.USER_REGISTER}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then(response => {
        console.log(response);
        return response.json();
      })
      .then(async user => {
        if (user._id) {
          console.log('USER:', user);

          await AsyncStorage.setItem(Keys.USER_KEY, JSON.stringify(user));

          this._onGrow(() => {
            this.props.navigation.navigate('Home', {user: user});
          });
        } else {
          Alert.alert(
            'Fehler bei der Anmeldung',
            `${user.errors.message}`,
            [
              {
                text: 'OK',
                onPress: () => {
                  this.setState({loading: false});
                  Animated.timing(this.buttonAnimated, {
                    toValue: 0,
                    duration: 200,
                    easing: Easing.linear,
                  }).start();
                  this.growAnimated.setValue(0);
                  this.spinValue.setValue(0);
                },
              },
            ],
            {cancelable: false},
          );
        }
      })
      .catch(error => {
        console.log(error);
        Alert.alert(
          'Fehler',
          'Es ist ein Fehler aufgetreten',
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({loading: false});
                Animated.timing(this.buttonAnimated, {
                  toValue: 0,
                  duration: 200,
                  easing: Easing.linear,
                }).start();
                this.growAnimated.setValue(0);
                this.spinValue.setValue(0);
              },
            },
          ],
          {cancelable: false},
        );
      });
  };

  render() {
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });

    const backToLoginButton = !this.state.loading ? (
      <View style={{alignSelf: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.replace('Login');
          }}>
          <Text style={{color: '#fff', textDecorationLine: 'underline'}}>
            Bereits registriert? Dann melde dich jetzt an!
          </Text>
        </TouchableOpacity>
      </View>
    ) : null;

    return (
      <ImageBackground
        source={require('../../assets/img/login_bg.png')}
        style={{width: '100%', height: '100%'}}>
        <KeyboardAvoidingView behavior="padding" style={styles.screen}>
          <StatusBar hidden={true} />
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              style={[styles.image]}
              source={require('../../assets/img/logo.png')}
            />
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'space-around',
              paddingHorizontal: 18,
            }}>
            <SafeAreaView>
              <View style={styles.input}>
                <TextInput
                  style={{color: 'white', height: 36}}
                  placeholder="Benutzername"
                  onChangeText={this._onChangeUsername}
                  autoCapitalize="none"
                  keyboardType="email-address"
                />
              </View>
              <View style={{height: 16}} />
              <View style={styles.input}>
                <TextInput
                  style={{color: 'white', height: 36}}
                  placeholder="Passwort"
                  secureTextEntry={true}
                  onChangeText={this._onChangePassword}
                />
              </View>
              <View style={{height: 16}} />
              <View style={styles.input}>
                <TextInput
                  style={{color: 'white', height: 36}}
                  placeholder="Passwort wiederholen"
                  secureTextEntry={true}
                  onChangeText={this._onChangePasswordRepeat}
                />
              </View>
              <View style={{height: 16}} />
              <Animated.View style={{width: changeWidth, alignSelf: 'center'}}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={this._onRegister}
                  activeOpacity={1}>
                  {this.state.loading ? (
                    <Animated.View style={{transform: [{rotate: spin}]}}>
                      <Icon name="spinner" size={30} color="#fff" />
                    </Animated.View>
                  ) : (
                    <Text style={styles.loginText}>REGISTRIEREN</Text>
                  )}
                </TouchableOpacity>
                <Animated.View
                  style={[styles.circle, {transform: [{scale: changeScale}]}]}
                />
              </Animated.View>
              <View style={{height: 16}} />
              {backToLoginButton}
            </SafeAreaView>
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 16,
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'rgba(255,255,255,0.4)',
    justifyContent: 'center',
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 16,
    height: 56,
  },
  screen: {
    flex: 1,
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#FFF',
    borderRadius: 26,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#69c3a2',
  },
  image: {
    resizeMode: 'cover',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.accent,
    height: MARGIN,
    borderRadius: 100,
    zIndex: 100,
  },
  loginText: {
    color: '#fff',
    backgroundColor: 'transparent',
  },
});
