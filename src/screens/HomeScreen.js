import React, {Component} from 'react';
import {StyleSheet, Text, View, StatusBar, Alert, Image} from 'react-native';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationActions} from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Level from '../components/Level';

const MaterialIconHeaderButton = props => (
  // the `props` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...props}
    IconComponent={MaterialIcons}
    iconSize={23}
    color="#FFF"
  />
);

export default class HomeScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'iRun',
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={MaterialIconHeaderButton}>
          <Item
            title="Abmelden"
            iconName="redo"
            onPress={navigation.getParam('logout')}
          />
        </HeaderButtons>
      ),
      headerLeft: (
        <HeaderButtons HeaderButtonComponent={MaterialIconHeaderButton}>
          <Item
            title="Öffne Menü"
            iconName="menu"
            onPress={navigation.getParam('openDrawer')}
          />
        </HeaderButtons>
      ),
    };
  };

  constructor(props) {
    super(props);

    console.log(props.navigation.getParam('user'));
    this.user = props.navigation.getParam('user');

    // Setze User Parameter im Drawer
    const setParamsAction = NavigationActions.setParams({
      params: {user: props.navigation.getParam('user')},
      key: 'App',
    });
    props.navigation.dispatch(setParamsAction);
  }

  componentDidMount() {
    // Für den Header-Button, damit dieser weiß, welche Methode er ausführen muss
    this.props.navigation.setParams({
      openDrawer: this._openDrawer,
      logout: this._logout,
    });
  }

  componentWillUnmount() {}

  render() {
    let altImg = {
      uri: `https://ui-avatars.com/api/?name=${this.user.username}&length=1`,
    };
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#545d5e" barStyle="light-content" />
        <ParallaxScrollView
          parallaxHeaderHeight={250}
          contentBackgroundColor="#ccc"
          bounces={false}
          renderBackground={() => (
            <Image
              style={styles.imgHeader}
              source={require('../assets/img/login_bg.png')}
            />
          )}
          renderForeground={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(0,0,0,0.75)',
              }}>
              <Image style={styles.imgProfile} source={altImg} />
              <Text style={{color: '#FFF', marginTop: 16, fontSize: 20}}>
                {this.user.username.toUpperCase()}
              </Text>
            </View>
          )}>
          <View
            style={{
              padding: 12,
              backgroundColor: '#ccc',
            }}>
            {/* Card1 */}
            <View
              style={{
                backgroundColor: '#fff',
                shadowColor: '#000',
                shadowOffset: {
                  width: 4,
                  height: 4,
                },
                shadowOpacity: 0.32,
                shadowRadius: 5.46,
                borderRadius: 8,
                elevation: 9,
              }}>
              <Text style={{margin: 16}}>INFO</Text>
              <View
                style={{
                  height: StyleSheet.hairlineWidth,
                  width: '100%',
                  backgroundColor: '#ccc',
                }}
              />
              <View style={{padding: 8}}>
                <Text>Hallo {this.user.username}!</Text>
                <Text>
                  Danke, dass du dich dafür entschieden hast, die App zu testen.
                  Bitte bedenke, dass es sich hier um einen Prototypen handelt.
                  Es kann sein, dass du auf Fehler stößt oder die App sogar
                  abstürzt. Bevor du aktiv deine Werte auf einer Strecke
                  analysieren kannst, muss du erstmal eine beliebige Strecke
                  aufzeichen. Du gehst dafür auf den Menüpunkt "Laufen" und
                  starte den Trackvorgang. Beachte bitte das Rundstrecken
                  erstmal nicht unterstützt werden und es somit beim Nachlaufen
                  zu Problemen kommt. Nachdem du fertig bist, lädst du diese
                  Aktivität auf den Server hoch. Absofort kannst du die Strecke
                  unter "Meine Strecken" ansehen und beliebig oft nachlaufen.
                  Hierfür begibst du dich zum Startpunkt der Strecke und drückst
                  auf den Start-Button. Die Zeit stoppt automatisch am Ziel.
                  Unter "Aktivitäten" kannst Du dir dann deine Statistik ansehen
                </Text>
              </View>
            </View>
            {/* Card2 */}
            <View
              style={{
                marginTop: 16,
                backgroundColor: '#fff',
                shadowColor: '#000',
                shadowOffset: {
                  width: 4,
                  height: 4,
                },
                shadowOpacity: 0.32,
                shadowRadius: 5.46,
                borderRadius: 8,
                elevation: 9,
              }}>
              <Text style={{margin: 16}}>Level</Text>
              <View
                style={{
                  height: StyleSheet.hairlineWidth,
                  width: '100%',
                  backgroundColor: '#ccc',
                }}
              />
              <View style={{padding: 16}}>
                <Level exp={this.user.exp} />
              </View>
            </View>
          </View>
        </ParallaxScrollView>
      </View>
    );
  }

  _openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  _logout = () => {
    Alert.alert(
      'Abmelden',
      'Willst du dich wirklich abmelden?',
      [
        {
          text: 'Abmelden',
          onPress: () => {
            this.setState({isLoggingOut: true}, async () => {
              await AsyncStorage.clear();
              this.props.navigation.navigate('Auth');
            });
          },
        },
        {
          text: 'Abbrechen',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgHeader: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
    backgroundColor: '#7f8c8d',
  },
  imgProfile: {
    height: 128,
    width: 128,
    borderRadius: 64,
    borderWidth: 4,
    borderColor: '#7f8c8d',
    resizeMode: 'cover',
  },
});
