import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import Colors from '../../config/Colors';
import API from '../../config/Api';
import NoActivities from '../../404Error/NoActivities';

export default class ActivityScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Aktivitäten',
      headerBackTitle: null,
    };
  };

  constructor(props) {
    super(props);
    this.user = props.navigation.getParam('user');
    this.state = {
      refreshing: true,
      completedTracks: [],
    };
  }

  componentDidMount() {
    this._fetchCompletedTracks();
  }

  render() {
    if (this.state.completedTracks.length == 0 && !this.state.refreshing) {
      return <NoActivities />;
    } else {
      return (
        <FlatList
          data={this.state.completedTracks}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({refreshing: true}, () => {
                  this._fetchCompletedTracks();
                });
              }}
              colors={[Colors.accent]}
              title="Aktualisiere"
              tintColor={Colors.accent}
              titleColor={Colors.accent}
            />
          }
          style={styles.container}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
        />
      );
    }
  }

  _fetchCompletedTracks = () => {
    fetch(`${API.BASE_URL}${API.TRACKS_COMPLETED_ROUTE}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${this.user.token}`,
      },
    })
      .then(resp => {
        console.log(resp);
        if (resp.status == 200) {
          return resp.json();
        } else {
          throw resp.status;
        }
      })
      .then(completedTracks => {
        console.log(completedTracks);
        if (completedTracks) {
          this.setState({completedTracks: completedTracks, refreshing: false});
        }
      })
      .catch(err => console.log(err));
  };

  _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('Statistic', {
            user: this.user,
            track: item,
          });
        }}>
        <View style={styles.itemContainer}>
          <View>
            <Text>{item.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
