import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, FlatList} from 'react-native';
// import {LineChart} from 'react-native-chart-kit';
import {
  Grid,
  AreaChart,
  LineChart,
  XAxis,
  YAxis,
} from 'react-native-svg-charts';
// import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import IRunFont from '../../components/IRunFont';
import Colors from '../../config/Colors';
import {Dropdown} from 'react-native-material-dropdown';
import moment from 'moment';
import 'moment/locale/de';
moment.locale('de');
import _ from 'lodash';
import {
  HeaderButtons,
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';

const IRunIconHeaderButton = props => (
  // the `props` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...props}
    IconComponent={IRunFont}
    iconSize={23}
    color="#FFF"
  />
);

export default class StatisticScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('track').activities[0].title,
      headerBackTitle: null,
      headerRight: (
        <HeaderButtons HeaderButtonComponent={IRunIconHeaderButton}>
          <Item
            title="Vergleichen"
            iconName="electrical_threshold"
            onPress={navigation.getParam('openTrackCompare')}
          />
        </HeaderButtons>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.track = props.navigation.getParam('track');
    this.user = props.navigation.getParam('user');

    console.log(this.track);

    this.rawX = [];
    this.rawY = [];

    this.track.trackProfile.forEach((element, index) => {
      this.rawX.push(element.distanceFromStart);
      this.rawY.push(element.elevation);
    });

    this.state = {
      // Aktivitäten kommen absteigend nach Datum sortiert rein
      activeActivityValue:
        this.track.activities.length > 1
          ? this.track.activities[1].title
          : this.track.activities[0].title,
      activeActivity:
        this.track.activities.length > 1
          ? this.track.activities[1]
          : this.track.activities[0],
    };

    console.log(this.rawY);
    console.log(this.rawX);
  }

  componentDidMount() {
    // Für den Header-Button, damit dieser weiß, welche Methode er ausführen muss
    this.props.navigation.setParams({
      openTrackCompare: this._openTrackCompare,
    });
  }

  _openTrackCompare = () => {
    this.props.navigation.navigate('TracksOfFriends', {
      user: this.user,
      userTrack: this.track,
      trackIntention: false,
    });
    // this.props.navigation.navigate('TrackCompare', {
    //   user: this.user,
    //   trackId: this.track._id,
    // });
  };

  render() {
    const axesSvg = {fontSize: 10, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10};
    const xAxisHeight = 30;
    const totalDistance = (this.track.totalDistance / 1000).toFixed(2);
    const elevationDifference = this.track.elevationDifference;
    const ascent = this.track.ascent;
    const descent = this.track.descent;
    const minElevation = this.track.minElevation;
    const maxElevation = this.track.maxElevation;

    const yMin = Math.floor(minElevation / 10) * 10;
    const yMax = Math.ceil(maxElevation / 10) * 10;

    console.log('yMin', yMin);
    console.log('yMax', yMax);

    return (
      <View style={styles.container}>
        <View style={{height: 200, padding: 10, flexDirection: 'row'}}>
          <YAxis
            data={this.rawY}
            style={{marginBottom: xAxisHeight}}
            contentInset={verticalContentInset}
            min={yMin}
            max={yMax}
            numberOfTicks={5}
            svg={axesSvg}
          />
          <View style={{flex: 1, marginLeft: 10}}>
            <AreaChart
              style={{flex: 1}}
              data={this.rawY}
              yMin={yMin}
              yMax={yMax}
              curve={shape.curveLinear}
              contentInset={verticalContentInset}
              svg={{
                fill: 'rgba(134, 65, 244, 0.5)',
                stroke: 'rgb(134, 65, 244)',
              }}>
              <Grid />
            </AreaChart>
            <XAxis
              style={{marginHorizontal: -10, height: xAxisHeight}}
              data={this.rawY}
              xAccessor={({index}) => {
                let maxVal = this.rawX.length < 5 ? this.rawX.length : 5;
                let delta = Math.floor(this.rawX.length / maxVal);

                console.log(
                  index % delta == 0
                    ? Math.round(this.rawX[index]) / 1000
                    : ' ',
                );

                return index % delta == 0
                  ? Math.round(this.rawX[index]) / 1000
                  : ' ';
              }}
              // formatLabel={(value, index) => {
              //   return Math.round(this.rawX[index]) / 1000;
              // }}
              contentInset={{left: 10, right: 10}}
              svg={axesSvg}
            />
          </View>
        </View>
        <View
          style={{
            height: 72,
            width: '100%',
            flexDirection: 'row',
            marginBottom: 8,
          }}>
          {/* Linker Container */}
          <View style={{flex: 1}}>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont name="road" color={Colors.primaryLight} size={20} />
                <Text style={styles.iconText}>{totalDistance} km</Text>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="average_math"
                  color={Colors.primaryLight}
                  size={20}
                />
                <Text style={styles.iconText}>{elevationDifference} m</Text>
              </View>
            </View>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont
                  name="bullish"
                  color={Colors.primaryLight}
                  size={20}
                />
                <Text style={styles.iconText}>{ascent} m</Text>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="bearish"
                  color={Colors.primaryLight}
                  size={20}
                />
                <Text style={styles.iconText}>{descent} m</Text>
              </View>
            </View>
            <View style={styles.iconContainer}>
              <View style={styles.iconRow}>
                <IRunFont
                  name="double_up"
                  color={Colors.primaryLight}
                  size={20}
                />
                <Text style={styles.iconText}>{maxElevation} m</Text>
              </View>
              <View style={styles.iconRow}>
                <IRunFont
                  name="double_down"
                  color={Colors.primaryLight}
                  size={20}
                />
                <Text style={styles.iconText}>{minElevation} m</Text>
              </View>
            </View>
          </View>
          {/* Ende linker Container */}
          {/* Rechter Container */}
          <View style={{flex: 1, flexDirection: 'column-reverse'}}>
            <Dropdown
              valueExtractor={(item, index) => {
                return item.title;
              }}
              onChangeText={(activity, idx, activities) => {
                this.setState({
                  activeActivityValue: activity,
                  activeActivity: activities[idx],
                });
              }}
              value={this.state.activeActivityValue}
              containerStyle={{marginHorizontal: 16}}
              dropdownOffset={{top: 0, left: 0}}
              data={this.track.activities}
            />
          </View>
        </View>
        <FlatList
          style={{
            borderTopWidth: StyleSheet.hairlineWidth,
            borderTopColor: '#ccc',
          }}
          data={this.track.activities[0].checkpointTimes}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={this._keyExtractor}
        />
      </View>
    );
  }

  _pad = n => (n < 10 ? '0' + n : n);

  _renderItem = ({item, index}) => {
    const duration = moment.duration(item.time);
    const centiseconds = Math.floor(duration.milliseconds() / 10);
    const timeString = `${this._pad(duration.minutes())}:${this._pad(
      duration.seconds(),
    )},${this._pad(centiseconds)}`;

    const selectedActivityCheckpointTimes = this.state.activeActivity
      .checkpointTimes;

    let cpDiff = moment(item.time).diff(
      moment(selectedActivityCheckpointTimes[index].time),
    );

    // Überschreibe Diff falls CP verpasst
    if (item.title == 'CP verpasst') cpDiff = 0;

    const duration2 = moment.duration(cpDiff);

    let isNegative = false;
    if (duration2.milliseconds() < 0 || duration2.seconds() < 0) {
      isNegative = true;
    }

    const centiseconds2 = isNegative
      ? Math.abs(Math.floor(duration2.milliseconds() / 10))
      : Math.floor(duration2.milliseconds() / 10);
    const diffTimeString = `${isNegative ? '-' : '+'} ${this._pad(
      Math.abs(duration2.seconds()),
    )},${this._pad(centiseconds2)}`;

    let averageSpeed = '-';
    // Berechne Tempo auf 100m
    if (index != 0 && index < this.track.activities[0].checkpointTimes.length) {
      let prevItem = this.track.activities[0].checkpointTimes[index - 1];
      let timeDiff = moment(item.time).diff(moment(prevItem.time)); // in ms
      let distance = item.distance - prevItem.distance;
      console.log(index, timeDiff, distance);

      let timeDiffSec = timeDiff / 1000;
      averageSpeed = distance / timeDiffSec; // Geschwindigkeit = Strecke / Zeit => m/s
    }

    return (
      <View style={styles.itemContainer}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <IRunFont name="place_marker" color={Colors.primaryLight} size={20} />
          <Text style={{marginLeft: 16}}>{item.title}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          {/* Durschnitttempo */}
          <View
            style={{flexDirection: 'row', alignSelf: 'center', marginRight: 8}}>
            <Text style={{fontSize: 9}}>Ø </Text>
            <Text style={{fontSize: 9}}>
              {averageSpeed != '-' ? averageSpeed.toFixed(2) : averageSpeed} m/s
            </Text>
          </View>
          {/* Zeit + Zeitdifferenz */}
          <View>
            <Text>{timeString}</Text>
            <Text
              style={[
                {alignSelf: 'flex-end', color: 'red'},
                isNegative && {color: 'green'},
              ]}>
              {diffTimeString}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  _renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  _keyExtractor = (item, index) => {
    return item._id;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconRow: {
    flexDirection: 'row',
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 4,
  },
  iconText: {
    color: Colors.primaryLight,
    marginLeft: 8,
  },
  rowContainer: {
    flexDirection: 'row',
    height: 24,
    width: '100%',
    alignItems: 'center',
  },
  itemContainer: {
    height: 56,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#ccc',
  },
});
