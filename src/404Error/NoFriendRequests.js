import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import IRunFont from "../components/IRunFont";
import Colors from "../config/Colors"

export default class NoFriendRequests extends Component {
    render() {
        return (
            <View style={styles.container}>
                <IRunFont style={{ marginBottom: 16 }} name="surprised" color={Colors.primaryLight} size={48} />
                <Text style={styles.text}>Du hast keine offnen Freundschaftsanfragen.</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        color: Colors.primaryLight,
        marginBottom: 4
    }
});