import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        title: 'Meine Strecken',
        icon: 'gesture',
        color: '#fff',
        onPress: this.openMyTracks,
      },
      {
        title: 'Laufen',
        icon: 'directions-run',
        color: '#fff',
        onPress: this.openTracker,
      },
      {
        title: 'Profil',
        icon: 'person',
        color: '#fff',
        onPress: null, //this.openProfile,
      },
      {
        title: 'Freunde',
        icon: 'group',
        color: '#fff',
        onPress: this.openFriendlist,
      },
      {
        title: 'Rangliste',
        icon: 'format-list-numbered',
        color: '#fff',
        onPress: this.openRanking,
      },
      {
        title: 'Aktivitäten',
        icon: 'gesture',
        color: '#fff',
        onPress: this.openActivity,
      },
    ];
  }

  openRanking = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('RankingTracks', { user: user });
  };

  openMyTracks = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('MyTracks', { user: user });
  };

  openTracker = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('Tracking', { user: user });
  };

  openProfile = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('Profile', { user: user });
  };

  openFriendlist = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('Friendlist', { user: user });
  };

  openActivity = () => {
    let user = this.props.navigation.getParam('user');
    this.props.navigation.navigate('Activity', { user: user });
  };

  keyExtractor = (item, index) => {
    return `${index}`;
  };

  renderMenuItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={item.onPress}
        style={{
          width: '100%',
          height: 96,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <MaterialIcons name={item.icon} size={30} color="#fff" />
        <Text style={{ color: '#fff', marginTop: 8 }}>{item.title}</Text>
      </TouchableOpacity>
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          width: 112,
          height: 1,
          backgroundColor: '#fff',
          alignSelf: 'center',
        }}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            width: '100%',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            style={{ width: 64, height: 64 }}
            source={require('./assets/img/logo.png')}
          />
        </View>
        <View
          style={{
            width: '100%',
            height: 4,
            backgroundColor: '#fff',
            marginBottom: 8,
          }}
        />
        <FlatList
          data={this.data}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderMenuItem}
        />
        {/* <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate("Test");
                    }}>
                        <Text>Hallo</Text>
                    </TouchableOpacity> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
