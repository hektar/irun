export default {

    primary: "#788587",
    primaryLight: "#aeb9b8",
    primaryDark: "#4a515d",
    accent: "#69c3a2",

    lightRed: "#f57c80",
    mustardYellow: "#edd18f"
}