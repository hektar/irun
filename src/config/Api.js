export default {
  BASE_URL: 'http://167.99.247.144',
  // BASE_URL: 'http://192.168.0.192',

  //----------Endpunkte----------------

  //Rangliste
  RANKINGS_ROUTE: '/api/rankings',

  //Strecken
  TRACKS_ROUTE: '/api/tracks',
  TRACKS_COMPLETED_ROUTE: '/api/tracks/completed',
  TRACKS_COMPARE: '/api/tracks/compare',
  TRACKS_FRIENDS: '/api/tracks/friends',

  //Aktivitäten
  ACTIVITIES_ROUTE: '/api/activities',

  //User
  USER_LOGIN: '/api/users/login',
  USER_REGISTER: '/api/users',
  USER_CURRENT: '/api/users/current',
  USER_ADD_FRIEND: '/api/users/addFriend',
  USER_FRIENDS: '/api/users/friends',
  USER_ACCEPT_FRIENDREQUEST: '/api/users/acceptFriend',
  USER_DECLINE_FRIENDREQUEST: '/api/users/declineFriend',
  //---------------------------------
};
