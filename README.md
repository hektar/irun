# iRun
iRun (c) 2019-2020 Tolga Kocer tolga.kocer.tk@gmail.com
## Installation
Um das Projekt zum Laufen zu kriegen, muss React Native installiert werden. Dafür folgt man am Besten den Anweisungen auf der React Native Webseite: 

https://facebook.github.io/react-native/docs/getting-started

Wichtig ist, dass nicht Expo, sondern "React Native CLI Quickstart" ausgewählt wird. MacOS als Betriebssytem wird empfohlen, da die iOS-App nicht auf Windows kompiliert werden kann.
Sind alle Schritte in der Installationsanleitung ausgeführt worden, muss man via Kommandozeile (Win) bzw. Terminal(Mac) im Projekt-Wurzelverzeichnis folgendes ausführen:

`$ npm install`

Damit werden alle abhängigen Module heruntergeladen. Danach führt man noch folgende Befehle aus (sofern man iOS kompilieren möchte):

`$ cd ios`

`$ pod install`

Nun kann die App gestartet werden:

`$ react-native run-ios`

oder

`$ react-native run-android` 

(Android-Simulator sollte vorher gestartet werden...am Besten man schließt ein Android-Smartphone an. USB-Debbuging auf dem Smartphone muss im Entwicklermenü eingeschaltet werden)

## Lizenz

Siehe [LICENSE.md](https://bitbucket.org/hektar/irun/src/master/LICENSE.md)